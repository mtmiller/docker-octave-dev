GNU Octave Docker development image
===================================

This repository provides a Docker image for building [GNU Octave]. Octave
depends on several libraries and build tools, all of which are included in
this image to make building it from source easier.

## Usage

This image can be used to build Octave, for example

    docker pull registry.gitlab.com/mtmiller/docker-octave-dev
    docker run registry.gitlab.com/mtmiller/docker-octave-dev sh -c "cd /tmp/octave && ./configure && make"

The base image is `ubuntu:bionic`.

## About Octave

[GNU Octave] is a high-level interpreted language, primarily intended for
numerical computations. It provides capabilities for the numerical solution of
linear and nonlinear problems, and for performing other numerical experiments.
It also provides extensive graphics capabilities for data visualization and
manipulation. Octave is normally used through its interactive command line
interface, but it can also be used to write non-interactive programs. The
Octave language is quite similar to Matlab so that most programs are easily
portable.

## Get Involved

This Docker image is maintained and built at
[mtmiller/docker-octave-dev on GitLab](https://gitlab.com/mtmiller/docker-octave-dev).

If you want to contribute in any way and help make this Docker image better,
please read the [contribution guidelines](CONTRIBUTING.md).

## License

The scripts used to build this Docker image are licensed under a modified BSD
license. See [LICENSE.md](LICENSE.md) for the full license text.

[GNU Octave] is free software: you can redistribute it and/or modify it under
the terms of the [GNU General Public License][gpl], either version 3 or any
later version.

[GNU Octave]: https://www.octave.org/
[gpl]: https://www.gnu.org/licenses/gpl-3.0.html
