## SPDX-License-Identifier: BSD-3-Clause
## Copyright (C) 2017-2020 Mike Miller
##
## GNU Octave Docker development image definition

FROM ubuntu:bionic

RUN apt-get update -y && apt-get install -y --no-install-recommends \
    autoconf \
    automake \
    bison \
    build-essential \
    curl \
    default-jdk \
    desktop-file-utils \
    epstool \
    fig2dev \
    flex \
    g++ \
    gawk \
    gcc \
    gfortran \
    ghostscript \
    git \
    gnuplot-qt \
    gperf \
    icoutils \
    info \
    less \
    libarpack2-dev \
    libblas-dev \
    libbz2-dev \
    libcurl4-gnutls-dev \
    libfftw3-dev \
    libfltk1.3-dev \
    libfontconfig1-dev \
    libfreetype6-dev \
    libgl1-mesa-dev \
    libgl2ps-dev \
    libglpk-dev \
    libglu1-mesa-dev \
    libgraphicsmagick++1-dev \
    libhdf5-dev \
    liblapack-dev \
    libncurses5-dev \
    libopenblas-dev \
    libpcre3-dev \
    libqhull-dev \
    libqrupdate-dev \
    libqscintilla2-qt5-dev \
    libqt5opengl5-dev \
    libreadline-dev \
    librsvg2-bin \
    libsndfile1-dev \
    libsuitesparse-dev \
    libsundials-dev \
    libtool \
    libumfpack5 \
    libxft-dev \
    lzip \
    make \
    mercurial \
    portaudio19-dev \
    pstoedit \
    qtbase5-dev \
    qtbase5-dev-tools \
    qttools5-dev \
    qttools5-dev-tools \
    texinfo \
    texlive-base \
    texlive-fonts-recommended \
    texlive-generic-recommended \
    texlive-latex-base \
    texlive-plain-generic \
    unzip \
    xauth \
    xz-utils \
    zip \
    zlib1g-dev \
  && apt-get clean \
  && rm -rf \
    /tmp/hsperfdata* \
    /var/*/apt/*/partial \
    /var/lib/apt/lists/* \
    /var/log/apt/term*
